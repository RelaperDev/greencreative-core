scoreboard objectives add cache dummy "缓存数据"
scoreboard objectives add information dummy
scoreboard objectives add flag dummy "权限等级"
scoreboard objectives add playtime dummy "游玩时间"
scoreboard objectives add cleaner dummy "积分数"
scoreboard objectives add uid dummy "用户编号"

scoreboard objectives setdisplay sidebar information

scoreboard players set @a[tag=grant] flag 4

team add blocked "被封禁"
team modify blocked suffix {"text": "|BANNED","color": "red","bold": true,"italic": true,"underlined": true}

scoreboard players set @a[tag=banned2] flag -1
scoreboard players set @a[tag=banned] flag -1

scoreboard players set @a[advancements={green:creative/super_dope=true},scores={playtime=..499}] playtime 500
scoreboard players set @a[advancements={green:creative/really_old=true},scores={playtime=..999}] playtime 1000 