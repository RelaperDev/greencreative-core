scoreboard players set @s cache 1440
execute as @a run execute if score @s uid = @a[scores={cache=1440,flag=..2},limit=1] make_mouse run scoreboard players set @s cache 1441
execute as @a[scores={cache=1441}] run execute if entity @s[scores={flag=3..}] run tellraw @a[scores={cache=1440}] {"text": "您没有权限对此用户使用星辰竹鼠","color": "red"}
execute as @a[scores={cache=1441}] run execute if entity @s[tag=grant] run tellraw @a[scores={cache=1440}] {"text": "您没有权限对此用户使用星辰竹鼠","color": "red"}
execute unless entity @a[scores={cache=1441}] run tellraw @s {"text":"该UID没有对应的用户，或者对应的用户不在线，或者您不能对此用户使用星辰竹鼠","color":"red"}
execute if entity @a[scores={cache=1441},tag=banned] run tellraw @s {"text":"该用户当前在封禁中，不能使用星辰竹鼠","color":"red"}
execute if entity @a[scores={cache=1441},tag=banned2] run tellraw @s {"text":"该用户当前在封禁中，不能使用星辰竹鼠","color":"red"}
execute if entity @a[scores={cache=1441},tag=rat] run tellraw @s {"text":"该用户已经是星辰竹鼠","color":"red"}
execute if entity @a[scores={cache=1441},tag=!grant,tag=!banned,tag=!banned2,tag=!rat] run tag @a[scores={cache=1441},tag=!grant,tag=!banned,tag=!banned2,limit=1,tag=!rat] add rat
execute if entity @a[scores={cache=1441},tag=rat] run tellraw @s "用户已标记为（或者已经是）星辰竹鼠"
scoreboard players set @a[scores={cache=1441}] cache 0
scoreboard players set @s make_mouse 0
scoreboard players set @s cache 0