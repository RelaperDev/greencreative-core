scoreboard players add banTimer cache 1

execute if score banTimer cache matches 4 run execute in minecraft:overworld run tp @a[tag=banned2,tag=!grant] -280 63 237
execute if score banTimer cache matches 4 run scoreboard players set banTimer cache 0

team join blocked @a[tag=banned2]
setblock -280 63 237 minecraft:end_portal
tag @a[tag=banned,tag=rat] remove rat
tag @a[tag=banned2,tag=rat] remove rat
tp @a[tag=!grant,x=-389,y=64,z=193,distance=..15,tag=!banned] -270 63 237
effect give @a[tag=banned] minecraft:wither 5 3
execute in minecraft:overworld run tp @a[tag=banned] -388.5 66 193
gamemode adventure @a[tag=banned]