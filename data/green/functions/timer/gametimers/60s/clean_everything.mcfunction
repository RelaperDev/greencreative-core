scoreboard players set 上次清理的物品 information 0
execute if entity @e[type=item] run execute store result score 上次清理的物品 information run kill @e[type=item]
execute if entity @e[type=experience_orb] run kill @e[type=experience_orb] 
execute if entity @e[type=arrow] run kill @e[type=arrow] 
execute if score 上次清理的物品 information matches 1.. run tellraw @a [{"text": "[","color": "dark_gray"},{"text": "控制台","bold": true,"color": "gold"},{"text": "]","bold": false,"color": "dark_gray"},{"text": " 我们帮你把掉落物清掉咯","color": "aqua"}]
execute if score 上次清理的物品 information matches 1.. as @a run playsound minecraft:block.note_block.harp master @s

effect give @a[tag=!noresist] resistance 60 255 true
fill -173 75 398 -223 68 424 air destroy