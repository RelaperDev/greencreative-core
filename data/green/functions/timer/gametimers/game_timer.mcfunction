scoreboard players add minuteTimer cache 1
execute if score minuteTimer cache matches 1200 run function #green:60s_functions
execute if score minuteTimer cache matches 1200 run scoreboard players set minuteTimer cache 0

scoreboard players add fiveMinuteTimer cache 1
execute if score fiveMinuteTimer cache matches 6000 run function #green:5min_functions
execute if score fiveMinuteTimer cache matches 6000 run scoreboard players set fiveMinuteTimer cache 0

scoreboard players add fourtyMinuteTimer cache 1
execute if score fourtyMinuteTimer cache matches 48000 run function #green:40min_functions
execute if score fourtyMinuteTimer cache matches 48000 run scoreboard players set fourtyMinuteTimer cache 0